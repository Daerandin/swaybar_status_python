# swaybar_status_python

This is just a script to be called by the bar part in the sway config. Just edit the default status_command to point to the get_stats.py file.

If you don't know what this should be used for, then you probably don't need this. But in short, the **sway** window manager for Linux, which runs on the **wayland** display server, has a status bar called **swaybar** and you can control what it displays via your own code. That is what this is.

NOTE: This is is unmaintained as I have moved my efforts over to implementing these features with C: [showstatus](https://gitlab.com/Daerandin/showstatus)
